Things to remember when writing a micronews for submission: 

* If you use a "#" symbol in the first word of Micronews, add a space before
it. Else markdown identifies it as heading. eg: " #debconf23 is in Kochi."
* two URLs in the same micronews causes issues with the bridge for twitter/X 
(only one of them is shown there) :/
* Use escape character '\' before the symbols like " between micronews. 
* To escape a single "!" on a line, instead of quotes use single quotes
to stop bash expansion. 
* Check your micronews prior to publishing to catch any formatting or other
errors via the 'make html && make serve' commands in your own repository. 


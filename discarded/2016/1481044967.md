Title: Number of cities that organized Debian Day by country and year https://wiki.debian.org/DebianDay#stats
Slug: 1481044967
Date: 2016-12-06 17:22
Author: Paulo Henrique de Lima Santana (phls) 
Status: published

Number of cities that organized Debian Day by country and year [https://wiki.debian.org/DebianDay#stats](https://wiki.debian.org/DebianDay#stats)

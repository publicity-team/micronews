Title: Crowdsourcing campaign to help packaging for Debian the Browserify node module (and its dependencies), by Pirate Praveen https://www.indiegogo.com/projects/package-browserify-node-module-for-debian
Slug: 1476355998
Date: 2016-10-13 10:53
Author: Laura Arjona Reina
Status: draft

Crowdsourcing campaign to help packaging for Debian the Browserify node module (and its dependencies), by Pirate Praveen [https://www.indiegogo.com/projects/package-browserify-node-module-for-debian](https://www.indiegogo.com/projects/package-browserify-node-module-for-debian)

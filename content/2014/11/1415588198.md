Title: Debian jessie architectures
Slug: 1415588198
Date: 2014-11-10 02:56:38
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/mFIXPs3rT4iXhjoFXEI2og

Debian jessie <a href="https://lists.debian.org/debian-devel-announce/2014/11/msg00005.html">adds</a> support for 64-bit PowerPC and ARM CPUs but removes the kFreeBSD architectures, which may have an unofficial release. The old Itanium, 32-bit S/390 and SPARC architectures are also removed.


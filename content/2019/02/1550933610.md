Title: This weekend we have a #Debian Bug Squashing #Party in Bonn, Germany https://wiki.debian.org/BSP/2019/02/de/Bonn #BSP
Slug: 1550933610
Date: 2019-02-23 14:53
Author: Laura Arjona Reina
Status: published

This weekend we have a #Debian Bug Squashing #Party in Bonn, Germany [https://wiki.debian.org/BSP/2019/02/de/Bonn](https://wiki.debian.org/BSP/2019/02/de/Bonn) #BSP

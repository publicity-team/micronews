Title: India Linux Users Group Delhi @ilugdelhi, Mozilla Delhi/NCR @mozilladelhincr and Debian Enthusiasts are organizing DebUtsav Delhi on March 9th and 10th, 2019 - Call for proposals open until Feb 25th https://lists.debian.org/debian-events-apac/2019/02/msg00000.html
Slug: 1550679702
Date: 2019-02-20 16:21
Author: Donald Norwood
Status: published

India Linux Users Group Delhi @ilugdelhi, Mozilla Delhi/NCR @mozilladelhincr and Debian Enthusiasts are organizing DebUtsav Delhi on March 9th and 10th, 2019 - Call for proposals open until Feb 25th [https://lists.debian.org/debian-events-apac/2019/02/msg00000.html](https://lists.debian.org/debian-events-apac/2019/02/msg00000.html)

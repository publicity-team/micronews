Title: MiniDebConf Hamburg June 8-9 after a three-day MiniDebCamp – Registration is open! https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg
Slug: 1554028899
Date: 2019-03-31 10:41
Author: Jean-Pierre Giraud
Status: published

MiniDebConf Hamburg June 8-9 after a three-day MiniDebCamp – Registration is open! [https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg](https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg)

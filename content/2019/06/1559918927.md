Title: Bits from the DPL (May 2019) https://lists.debian.org/debian-devel-announce/2019/06/msg00000.html
Slug: 1559918927
Date: 2019-06-07 14:48
Author: Laura Arjona Reina
Status: published

Bits from the DPL (May 2019) [https://lists.debian.org/debian-devel-announce/2019/06/msg00000.html](https://lists.debian.org/debian-devel-announce/2019/06/msg00000.html)

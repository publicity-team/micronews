Title: 3 days until #DebianDay! There is still time to organize a local celebration of The universal operating system. https://wiki.debian.org/DebianDay/2019  (Artwork by Juliana Krauss)
Slug: 1565728615
Date: 2019-08-13 20:36
Author: Donald Norwood
Status: published

3 days until #DebianDay! There is still time to organize a local celebration of The universal operating system. [https://wiki.debian.org/DebianDay/2019](https://wiki.debian.org/DebianDay/2019)  (Artwork by Juliana Krauss)

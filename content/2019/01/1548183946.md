Title: Important APT security update - please read the instructions to upgrade APT safely https://www.debian.org/security/2019/dsa-4371
Slug: 1548183946
Date: 2019-01-22 19:05
Author: Laura Arjona Reina
Status: published

Important APT security update - please read the instructions to upgrade APT safely [https://www.debian.org/security/2019/dsa-4371](https://www.debian.org/security/2019/dsa-4371)

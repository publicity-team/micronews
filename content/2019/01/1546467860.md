Title: We are getting ready! The Transition freeze begins 01-12, during this freeze, packages with failing autopkgtests are blocked from testing. This is the 1st of 3 freeze periods toward the release of Debian 10 (codename: buster). https://release.debian.org/buster/freeze_policy.html
Slug: 1546467860
Date: 2019-01-02 22:24
Author: Donald Norwood
Status: published

We are getting ready! The Transition freeze begins 01-12, during this freeze, packages with failing autopkgtests are blocked from testing. This is the 1st of 3 freeze periods toward the release of Debian 10 (codename: buster). [https://release.debian.org/buster/freeze_policy.html](https://release.debian.org/buster/freeze_policy.html)

Title: Short talks in the Auditorium: "Debian on the Raspberryscape" followed by "Cat-Herding Development Boards" https://debconf19.debconf.org/schedule/venue/1/ #DebConf19
Slug: 1564082364
Date: 2019-07-25 19:19
Author: Laura Arjona Reina
Status: published

Short talks in the Auditorium: "Debian on the Raspberryscape" followed by "Cat-Herding Development Boards" [https://debconf19.debconf.org/schedule/venue/1/](https://debconf19.debconf.org/schedule/venue/1/) #DebConf19

Title: Debian stretch is not forgotten in the excitement - it will receive a further 12 months support from the Debian Security Team, and then they will hand over to the LTS and ELTS teams #releasingDebianBuster
Slug: 1562466007
Date: 2019-07-07 02:20
Author: Jonathan Wiltshire
Status: published

Debian stretch is not forgotten in the excitement - it will receive a further 12 months support from the Debian Security Team, and then they will hand over to the LTS and ELTS teams #releasingDebianBuster

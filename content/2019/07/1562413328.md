Title: There will be more than 37 Debian buster release parties in 23 different countries and some of them already started https://wiki.debian.org/ReleasePartyBuster #ReleasingDebianBuster
Slug: 1562413328
Date: 2019-07-06 11:42
Author: Laura Arjona Reina
Status: published

There will be more than 37 Debian buster release parties in 23 different countries and some of them already started [https://wiki.debian.org/ReleasePartyBuster](https://wiki.debian.org/ReleasePartyBuster) #ReleasingDebianBuster

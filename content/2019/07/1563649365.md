Title: "Introdução de Git e GitLab" workshop happening now in #DebConf19 Open Day https://debconf19.debconf.org/talks/18-introducao-de-git-e-gitlab/
Slug: 1563649365
Date: 2019-07-20 19:02
Author: Laura Arjona Reina
Status: published

"Introdução de Git e GitLab" workshop happening now in #DebConf19 Open Day [https://debconf19.debconf.org/talks/18-introducao-de-git-e-gitlab/](https://debconf19.debconf.org/talks/18-introducao-de-git-e-gitlab/)

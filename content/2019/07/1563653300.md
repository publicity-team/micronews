Title: DebConf19 convida você para o Dia Aberto do Debian na Universidade Federal de Tecnologia do Paraná (UTFPR), em Curitiba https://bits.debian.org/2019/07/debconf19-open-day-pt-BR.html
Slug: 1563653300
Date: 2019-07-20 20:08
Author: Laura Arjona Reina
Status: published

DebConf19 convida você para o Dia Aberto do Debian na Universidade Federal de Tecnologia do Paraná (UTFPR), em Curitiba [https://bits.debian.org/2019/07/debconf19-open-day-pt-BR.html](https://bits.debian.org/2019/07/debconf19-open-day-pt-BR.html)

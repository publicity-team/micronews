Title: "Our revels now are ended" - William Shakespare. We come together this evening for our closing ceremony which will be held in the Auditório in a few minutes. https://debconf19.debconf.org/schedule/venue/1/
Slug: 1564264615
Date: 2019-07-27 21:56
Author: Donald Norwood
Status: published

"Our revels now are ended" - William Shakespare. We come together this evening for our closing ceremony which will be held in the Auditório in a few minutes. [https://debconf19.debconf.org/schedule/venue/1/](https://debconf19.debconf.org/schedule/venue/1/)

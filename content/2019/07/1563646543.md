Title:  #DebConf19 invites you to Debian Open Day at the Federal University of Technology - Paraná (UTFPR), in Curitiba https://bits.debian.org/2019/07/debconf19-open-day.html
Slug: 1563646543
Date: 2019-07-20 18:15
Author: Laura Arjona Reina
Status: published

 #DebConf19 invites you to Debian Open Day at the Federal University of Technology - Paraná (UTFPR), in Curitiba [https://bits.debian.org/2019/07/debconf19-open-day.html](https://bits.debian.org/2019/07/debconf19-open-day.html)

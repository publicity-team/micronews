Title: Last set of talks and discussion sessions at #DebConf19 today: "The journey towards being a FOSS-only artist through Debian GNU/Linux", "Finding the bits and pieces for RISC-V in Debian" and the Xen BoF will be streamed. Meanwhile, "Assembléia Geral do ICTL" in A102 (no video, sorry) https://debconf19.debconf.org/schedule/?day=2019-07-26
Slug: 1564177102
Date: 2019-07-26 21:38
Author: Donald Norwood
Status: published

Last set of talks and discussion sessions at #DebConf19 today: "The journey towards being a FOSS-only artist through Debian GNU/Linux", "Finding the bits and pieces for RISC-V in Debian" and the Xen BoF will be streamed. Meanwhile, "Assembléia Geral do ICTL" in A102 (no video, sorry) [https://debconf19.debconf.org/schedule/?day=2019-07-26](https://debconf19.debconf.org/schedule/?day=2019-07-26)

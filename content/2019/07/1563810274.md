Title: The morning continues with "Caninos Loucos: Enabling Design and Manufacture of FOSSH in Latin America", "What's new in the Linux kernel (and what's missing in Debian)" and the DebConf Bursaries BoF https://debconf19.debconf.org/schedule/?day=2019-07-22 #DebConf19
Slug: 1563810274
Date: 2019-07-22 15:44
Author: Laura Arjona Reina
Status: published

The morning continues with "Caninos Loucos: Enabling Design and Manufacture of FOSSH in Latin America", "What's new in the Linux kernel (and what's missing in Debian)" and the DebConf Bursaries BoF [https://debconf19.debconf.org/schedule/?day=2019-07-22](https://debconf19.debconf.org/schedule/?day=2019-07-22) #DebConf19

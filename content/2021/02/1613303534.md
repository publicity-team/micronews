Title: Call for mentors and project ideas for next Google Summer of code and Outreachy rounds https://lists.debian.org/debian-outreach/2021/02/msg00005.html
Slug: 1613303534
Date: 2021-02-14 11:52
Author: Jean-Pierre Giraud
Status: published

Call for mentors and project ideas for next Google Summer of Code and Outreachy rounds [https://lists.debian.org/debian-outreach/2021/02/msg00005.html](https://lists.debian.org/debian-outreach/2021/02/msg00005.html)

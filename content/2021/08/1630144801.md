Title: The last day at #DebConf21 online starts at 12:00 UTC with a talk about "GoVarnam, a new 'Intelligent' Input Method for Indian Languages in Desktop & Mobile" and, on channel 2, the first of a series of talks in Hindi: "कोई भी मोबाइल उपयोगकर्ता मुक्त/स्वतन्त्र सॉफ्टवेयर में कैसे स्थानांतरित कर सकता है "(How can any mobile user migrate to free software), https://debconf21.debconf.org/schedule/?block=5
Slug: 1630144801
Date: 2021-08-28 10:00
Author: Jean-Pierre Giraud
Status: published

The last day at #DebConf21 online starts at 12:00 UTC with a talk about "GoVarnam, a new 'Intelligent' Input Method for Indian Languages in Desktop & Mobile" and, on channel 2, the first of a series of talks in Hindi: "कोई भी मोबाइल उपयोगकर्ता मुक्त/स्वतन्त्र सॉफ्टवेयर में कैसे स्थानांतरित कर सकता है "(How can any mobile user migrate to free software), [https://debconf21.debconf.org/schedule/?block=5](https://debconf21.debconf.org/schedule/?block=5)

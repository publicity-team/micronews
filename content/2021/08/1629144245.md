Title: The Debian cloud team provides images for Amazon EC2, Microsoft Azure, OpenStack and plain VM, and will update the images until the end of the LTS period for bullseye. New images are typically released for each point release and after security fixes for critical packages. Detailed info in https://cloud.debian.org #NewInDebianBullseye #ReleasingDebianBullseye
Slug: 1629144245
Date: 2021-08-16 20:04
Author: Laura Arjona Reina
Status: published

The Debian cloud team provides images for Amazon EC2, Microsoft Azure, OpenStack and plain VM, and will update the images until the end of the LTS period for bullseye. New images are typically released for each point release and after security fixes for critical packages. Detailed info in [https://cloud.debian.org](https://cloud.debian.org) #NewInDebianBullseye #ReleasingDebianBullseye

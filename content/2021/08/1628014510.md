Title: Debian has a virtual booth at #GitLabCommit 2021. Come and visit us, live chat session today 3 August at 16:45 UTC (in about 15 min!) https://gitlabcommitvirtual2021.com/
Slug: 1628014510
Date: 2021-08-03 18:15
Author: Laura Arjona Reina
Status: published

Debian has a virtual booth at #GitLabCommit 2021. Come and visit us, live chat session today 3 August at 16:45 UTC (in about 15 min!) [https://gitlabcommitvirtual2021.com/](https://gitlabcommitvirtual2021.com/)

Title:  #DebConf21 will be back at 20:00 for the talk 'Probing into the world of Data Science': all you want to know about Data Science and the current Data Science tools that exist in Debian, https://debconf21.debconf.org/schedule/?block=3
Slug: 1630015025
Date: 2021-08-26 21:57
Author: Jean-Pierre Giraud
Status: published

 #DebConf21 will be back at 20:00 for the talk 'Probing into the world of Data Science': all you want to know about Data Science and the current Data Science tools that exist in Debian, [https://debconf21.debconf.org/schedule/?block=3](https://debconf21.debconf.org/schedule/?block=3)

Title: At 17:00 UTC, The BoF of the Debian Community Team will focuse on who they are and what they do. If you have any questions, please do not hesitate to ask, https://debconf21.debconf.org/schedule/venue/1/
Slug: 1629826905
Date: 2021-08-24 17:41
Author: Jean-Pierre Giraud
Status: published

At 17:00 UTC, The BoF of the Debian Community Team will focuse on who they are and what they do. If you have any questions, please do not hesitate to ask, [https://debconf21.debconf.org/schedule/venue/1/](https://debconf21.debconf.org/schedule/venue/1/)

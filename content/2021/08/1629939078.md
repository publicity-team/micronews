Title: Day 2 of #DebConf2021 has ended. Tune in again tomorrow Thursday, August 26 at 12:00 UTC. The schedule for the day is https://debconf21.debconf.org/schedule/?block=3 Thank you to all of our contributors, viewers, and to our Debconf Video team! See you tomorrow!
Slug: 1629939078
Date: 2021-08-26 00:51
Author: Laura Arjona Reina
Status: published

Day 2 of #DebConf2021 has ended. Tune in again tomorrow Thursday, August 26 at 12:00 UTC. The schedule for the day is [https://debconf21.debconf.org/schedule/?block=3](https://debconf21.debconf.org/schedule/?block=3) Thank you to all of our contributors, viewers, and to our Debconf Video team! See you tomorrow!

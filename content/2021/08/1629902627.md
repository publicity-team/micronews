Title: The second day at #DebConf21 will start at 15:00 UTC on channel one with the BoF 'Growing the Debian Community in India' at the same time as a two hours workshop which is open to everyone interested in doing some 'Live packaging' of software, on channel two, https://debconf21.debconf.org/schedule/?block=2
Slug: 1629902627
Date: 2021-08-25 14:43
Author: Jean-Pierre Giraud
Status: published

The second day at #DebConf21 will start at 15:00 UTC on channel one with the BoF 'Growing the Debian Community in India' at the same time as a two hours workshop which is open to everyone interested in doing some 'Live packaging' of software, on channel two, [https://debconf21.debconf.org/schedule/?block=2](https://debconf21.debconf.org/schedule/?block=2)

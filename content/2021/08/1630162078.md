Title: On channel two, the Hindi track continues at 13:00 UTC with three short talks: "हिंदी में अनुवाद करने के लिए कार्यप्रणाली" (How the translation workflow for Hindi works), "Gaming in Debian" explaining the ease of developing games in Debian (13:30), and डेबियन में योगदान कैसे करें (How to Contribute to Debian) at 14:00 UTC, https://debconf21.debconf.org/schedule/venue/2/
Slug: 1630162078
Date: 2021-08-28 14:47
Author: Laura Arjona Reina
Status: published

On channel two, the Hindi track continues at 13:00 UTC with three short talks: "हिंदी में अनुवाद करने के लिए कार्यप्रणाली" (How the translation workflow for Hindi works), "Gaming in Debian" explaining the ease of developing games in Debian (13:30), and डेबियन में योगदान कैसे करें (How to Contribute to Debian) at 14:00 UTC, [https://debconf21.debconf.org/schedule/venue/2/](https://debconf21.debconf.org/schedule/venue/2/)

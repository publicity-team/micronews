Title: We have organised a small Poetry Event in #DebConf21 during the evening break (18:45 UTC). If you have some poems you'd like to share (yours or from a poet you like, no matter the language), follow the instructions to join us https://debconf21.debconf.org/talks/111-poetry-event/
Slug: 1630170776
Date: 2021-08-28 17:12
Author: Laura Arjona Reina
Status: published

We have organised a small Poetry Event in #DebConf21 during the evening break (18:45 UTC). If you have some poems you'd like to share (yours or from a poet you like, no matter the language), follow the instructions to join us [https://debconf21.debconf.org/talks/111-poetry-event/](https://debconf21.debconf.org/talks/111-poetry-event/)

Title:  #DebConf21 will shortly be over, so we shall console ourselves with (notional) cheese and wine. Bring your own cheese, we’ll provide the company for the Closing Ceremony and Cheese and Wine party! https://debconf21.debconf.org/schedule/venue/1/
Slug: 1630182797
Date: 2021-08-28 20:33
Author: Laura Arjona Reina
Status: published

 #DebConf21 will shortly be over, so we shall console ourselves with (notional) cheese and wine. Bring your own cheese, we’ll provide the company for the Closing Ceremony and Cheese and Wine party! [https://debconf21.debconf.org/schedule/venue/1/](https://debconf21.debconf.org/schedule/venue/1/)

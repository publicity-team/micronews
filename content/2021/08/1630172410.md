Title: Are you participating in #DebConf21? Wether you are a speaker or help in the video team, you're watching the stream or participating in the IRC channels, we'd like you to be in the DebConf21 group photo! Follow the instructions to send your photo before DebConf finishes (today!) https://lists.debian.org/debconf-announce/2021/08/msg00002.html
Slug: 1630172410
Date: 2021-08-28 17:40
Author: Laura Arjona Reina
Status: published

Are you participating in #DebConf21? Wether you are a speaker or help in the video team, you're watching the stream or participating in the IRC channels, we'd like you to be in the DebConf21 group photo! Follow the instructions to send your photo before DebConf finishes (today!) [https://lists.debian.org/debconf-announce/2021/08/msg00002.html](https://lists.debian.org/debconf-announce/2021/08/msg00002.html)

Title:  #DebConf21 online continues at 16:00 UTC with a talk in Telugu "డెబియన్ ప్రాజెక్ట్ - అంతర్జాతీయ ఆపరేటింగ్ సిస్టంగా దాని సౌలబ్యథ మరియు రుపాంతర" about Debian Project as international operating system on channel two; at the same time the short talk "Debuginfod on Debian" in channel one explains what is a debuginfod server and how to use it, followed at 16:30 UTC by the talk "Two Pinebooks walk into a bar..." telling the tale of two arm64 laptops, https://debconf21.debconf.org/schedule/?block=5
Slug: 1630172710
Date: 2021-08-28 17:45
Author: Laura Arjona Reina
Status: published

 #DebConf21 online continues at 16:00 UTC with a talk in Telugu "డెబియన్ ప్రాజెక్ట్ - అంతర్జాతీయ ఆపరేటింగ్ సిస్టంగా దాని సౌలబ్యథ మరియు రుపాంతర" about Debian Project as international operating system on channel two; at the same time the short talk "Debuginfod on Debian" in channel one explains what is a debuginfod server and how to use it, followed at 16:30 UTC by the talk "Two Pinebooks walk into a bar..." telling the tale of two arm64 laptops, [https://debconf21.debconf.org/schedule/?block=5](https://debconf21.debconf.org/schedule/?block=5)

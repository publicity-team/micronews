Title: MiniDebCamp Tilburg (Nederlands) is happening this weekend, visit their page to have a look at their activities and the group photo, and join if you happen to be near there  https://wiki.debian.org/DebianEvents/nl/2021/MiniDebCamp21Tilburg
Slug: 1630235006
Date: 2021-08-29 11:03
Author: Laura Arjona Reina
Status: published

MiniDebCamp Tilburg (Nederlands) is happening this weekend, visit their page to have a look at their activities and the group photo, and join if you happen to be near there  [https://wiki.debian.org/DebianEvents/nl/2021/MiniDebCamp21Tilburg](https://wiki.debian.org/DebianEvents/nl/2021/MiniDebCamp21Tilburg)

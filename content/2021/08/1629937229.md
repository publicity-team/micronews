Title: The last English talk for Wednesday at #DebConf21 is 'Detecting At-Risk Projects in Debian' at 23:00 UTC on channel one https://debconf21.debconf.org/schedule/venue/1/
Slug: 1629937229
Date: 2021-08-26 00:20
Author: Laura Arjona Reina
Status: published

The last English talk for Wednesday at #DebConf21 is 'Detecting At-Risk Projects in Debian' at 23:00 UTC on channel one [https://debconf21.debconf.org/schedule/venue/1/](https://debconf21.debconf.org/schedule/venue/1/)

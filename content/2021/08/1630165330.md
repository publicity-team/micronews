Title: The short talk "Debian for Open Science" (14:00 UTC) is intended to put forth the idea and the means for Debian packages for Open Science and "Looking Forward to Reproducible Builds" (14:30 UTC) will highlight some recent developments, with an eye to what might happen as Debian embarks upon the "Bookworm" development cycle. Both talks on channel one, https://debconf21.debconf.org/schedule/venue/1/
Slug: 1630165330
Date: 2021-08-28 15:42
Author: Laura Arjona Reina
Status: published

The short talk "Debian for Open Science" (14:00 UTC) is intended to put forth the idea and the means for Debian packages for Open Science and "Looking Forward to Reproducible Builds" (14:30 UTC) will highlight some recent developments, with an eye to what might happen as Debian embarks upon the "Bookworm" development cycle. Both talks on channel one, [https://debconf21.debconf.org/schedule/venue/1/](https://debconf21.debconf.org/schedule/venue/1/)

Title: From 21:45 UTC through 23:00 UTC #DebConf21 pauses for a break: use this time to watch sessions you missed at https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/
Slug: 1630018817
Date: 2021-08-26 23:00
Author: Jean-Pierre Giraud
Status: published

From 21:45 UTC through 23:00 UTC #DebConf21 pauses for a break: use this time to watch sessions you missed at [https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/](https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/)

Title: Regexp matching support is now compiled into journalctl so that it is always available in Debian 11  #NewInDebianBullseye #ReleasingDebianBullseye
Slug: 1628988935
Date: 2021-08-15 00:55
Author: Donald Norwood
Status: published

Regexp matching support is now compiled into journalctl so that it is always available in Debian 11  #NewInDebianBullseye #ReleasingDebianBullseye

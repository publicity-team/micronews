Title: The last talks for Tuesday at #DebConf21 are 'Reproducible Buster, Bullseye & Bookworm - where we come from and where we are going' at 21:00 UTC and the BoF 'Lenovo + Debian - 2021' about the current progress of the Lenovo Linux program at 22:00 UTC, https://debconf21.debconf.org/schedule/?block=1
Slug: 1629842727
Date: 2021-08-24 22:05
Author: Jean-Pierre Giraud
Status: published

The last talks for Tuesday at #DebConf21 are 'Reproducible Buster, Bullseye & Bookworm - where we come from and where we are going' at 21:00 UTC and the BoF 'Lenovo + Debian - 2021' about the current progress of the Lenovo Linux program at 22:00 UTC, [https://debconf21.debconf.org/schedule/?block=1](https://debconf21.debconf.org/schedule/?block=1)

Title: debian-edu-doc - call for translations and translation updates of the Debian Edu bullseye manual https://lists.debian.org/debian-i18n/2021/02/msg00002.html
Slug: 1615387126
Date: 2021-03-10 14:58
Author: Paulo Henrique de Lima Santana (phls)
Status: published

debian-edu-doc - call for translations and translation updates of the Debian Edu bullseye manual [https://lists.debian.org/debian-i18n/2021/02/msg00002.html](https://lists.debian.org/debian-i18n/2021/02/msg00002.html)

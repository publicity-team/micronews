Title: Debian progress on bullseye reaches Milestone 3: The Hard Freeze. This stage puts us a step closer to the full release in the near future. https://lists.debian.org/debian-devel-announce/2021/03/msg00006.html #debianbullseye
Slug: 1616269240
Date: 2021-03-20 19:40
Author: Donald Norwood
Status: published

Debian progress on bullseye reaches Milestone 3: The Hard Freeze. This stage puts us a step closer to the full release in the near future. [https://lists.debian.org/debian-devel-announce/2021/03/msg00006.html](https://lists.debian.org/debian-devel-announce/2021/03/msg00006.html) #debianbullseye

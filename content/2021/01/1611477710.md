Title: Afternoon session in Buzz starts with a workshop: "Making your own Debian Distro", followed by Lightning talks, "Portable and repeatable Linux kernel builds with TuxMake", talk on "pristine-lfs: a robust replacement for pristine-tar", "Mass Campaigns to increase Debian adoption in India" and "Debian in Indian Institutions" #MiniDebConf #debian #freesoftware #DebianIndia #MDCO-IN-2021
Slug: 1611477710
Date: 2021-01-24 08:41
Author: Anupa Ann Joseph
Status: published

Afternoon session in Buzz starts with a workshop: "Making your own Debian Distro", followed by Lightning talks, "Portable and repeatable Linux kernel builds with TuxMake", talk on "pristine-lfs: a robust replacement for pristine-tar", "Mass Campaigns to increase Debian adoption in India" and "Debian in Indian Institutions" #MiniDebConf #debian #freesoftware #DebianIndia #MDCO-IN-2021

Title: Proxmox, provider of powerful, yet easy-to-use Open Source server software based on Debian is the newest Platinum Sponsor of #DebConf25 https://bits.debian.org/2025/02/proxmox-platinum-debconf25.html #debian
Slug: 1738862976
Date: 2025-02-06 17:29
Author: Jean-Pierre Giraud
Status: published

Proxmox, provider of powerful, yet easy-to-use Open Source server software based on Debian is the newest Platinum Sponsor of #DebConf25 [https://bits.debian.org/2025/02/proxmox-platinum-debconf25.html](https://bits.debian.org/2025/02/proxmox-platinum-debconf25.html) #debian

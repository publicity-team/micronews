Title: Updated Debian 12: 12.9 released https://www.debian.org/News/2025/20250111 #debian #bookworm
Slug: 1736606183
Date: 2025-01-11 14:36
Author: Anupa Ann Joseph
Status: published

Updated Debian 12: 12.9 released [https://www.debian.org/News/2025/20250111](https://www.debian.org/News/2025/20250111) #debian #bookworm

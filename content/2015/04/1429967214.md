Title: Releasing Debian jessie: themes
Slug: 1429967214
Date: 2015-04-25 13:06:54
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/1PKqQywwTLyAj63-6HGREA

The Debian Jessie desktop theme is &quot;<a href="https://wiki.debian.org/DebianArt/Themes/Lines">Lines</a>&quot;, made by <a href="https://wiki.debian.org/JulietteTaka">Juliette Taka Belin</a>. #releasingjessie


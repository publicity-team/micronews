Title: Releasing Debian jessie: Android
Slug: 1430044081
Date: 2015-04-26 10:28:01
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/uhPhhDBKT_mLFatLquK5BQ

Debian jessie includes <a href="https://wiki.debian.org/AndroidTools">tools related to Android</a>, including parts of the SDK. More tools will be available in stretch. #releasingjessie


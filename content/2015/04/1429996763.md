Title: Releasing Debian jessie: waiting for 3 more CD sets
Slug: 1429996763
Date: 2015-04-25 21:19:23
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/lIf62nPfSU6tEPd05AVmQQ

Only 3 more sets of CDs to produce and test, after that, we QA for one last time, and publish the final images! #releasingjessie


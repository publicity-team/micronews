Title: Releasing Debian jessie: amd64 images
Slug: 1429971612
Date: 2015-04-25 14:20:12
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/hDD-HL2qQKKbV6bK6p4CAA

The Debian CD build server has built the amd64 images and they are <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting/Jessie">in place for testing</a>. #releasingjessie


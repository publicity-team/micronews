Title: Releasing Debian jessie: DebConf
Slug: 1430040368
Date: 2015-04-26 09:26:08
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/2GQq77k9Svq88UIAaHIaRg

Debian would like to thank our DebConf partners; 30 sponsors, 7 infrastructure sponsors, 2 media partners and 2 organisational partners <a href="http://debconf15.debconf.org/sponsors.xhtml">http://debconf15.debconf.org/sponsors.xhtml</a> #releasingjessie


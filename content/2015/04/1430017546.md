Title: Releasing Debian jessie: Official announcement of Debian 8 release https://www.debian.org/News/2015/20150426
Slug: 1430017546
Date: 2015-04-26 03:05:46
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/qF_6maL4Rc62YXD4y001Ag

Debian 8 "Jessie" officially released. [https://www.debian.org/News/2015/20150426](https://www.debian.org/News/2015/20150426)


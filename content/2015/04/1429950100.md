Title: Releasing Debian jessie: hints
Slug: 1429950100
Date: 2015-04-25 08:21:40
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/O-9VT-9hT0SZmHRKcdTN0A

The Debian release team has applied about 2100 hints (e.g. unblocks/manual removals) during the jessie freeze, about 2/3 used for wheezy. #releasingjessie


Title: Releasing Debian jessie: Volume of updates and removes.
Slug: 1430023155
Date: 2015-04-26 04:39:15
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/y0QckESMTnG5q-FB5VfGuw

The awesome Debian release team handled almost 3000 requests to update/remove/recompile packages for Debian Jessie.


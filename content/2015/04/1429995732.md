Title: Releasing Debian jessie: consultants
Slug: 1429995732
Date: 2015-04-25 21:02:12
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/wBu9njCpRSmnlEWDWyx3bA

There are 375 Debian <a href="https://www.debian.org/consultants/">consultants</a> in 53 countries worldwide. Consult on Debian today! #releasingjessie


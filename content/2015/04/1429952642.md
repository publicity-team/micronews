Title: Releasing Debian jessie: lots of source code
Slug: 1429952642
Date: 2015-04-25 09:04:02
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/UYPGKHOtS_2NwS7OyjLe4A

Debian jessie has 21,087 source packages with 8,359,525 source files. See more stats (languages, other) at <a href="http://sources.debian.net/stats/jessie/">http://sources.debian.net/stats/jessie/ </a>#releasingjessie


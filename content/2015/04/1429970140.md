Title: Releasing Debian jessie: contributed services
Slug: 1429970140
Date: 2015-04-25 13:55:40
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/63lVToa4Rp6WkvDENZSDtw

There are at least 83 different Debian-related <a href="https://wiki.debian.org/Services">services</a> run by Debian contributors. #releasingjessie


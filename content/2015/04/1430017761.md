Title: Releasing Debian jessie: Helpful tips on upgrading from Wheezy.
Slug: 1430017761
Date: 2015-04-26 03:09:21
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/mbWtS3h-QEKmF-ikOeXdcw

When upgrading from Wheezy to Jessie, it is be a good idea to purge old packages before the first reboot.


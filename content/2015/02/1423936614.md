Title: Free Software Love
Slug: 1423936614
Date: 2015-02-14 17:56:54
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/wxJBt5GNR4ix2j58x2eObQ

We love you: Our users, contributors, upstreams and downstreams, whoever gives us their time, or who benefits from our work, and of course, the rest of the Free Software universe! #ilovefs


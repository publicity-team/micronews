Title: Registration for DebConf15 is now open!
Slug: 1425443463
Date: 2015-03-04 04:31:03
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/8nEcJRmJTCyXpuD8fbsiHQ

Registration for DebConf15 in August in Heidelberg (DE) is now open: <a href="https://lists.debian.org/debian-events-eu/2015/03/msg00000.html">https://lists.debian.org/debian-events-eu/2015/03/msg00000.html</a> We hope to see you there!


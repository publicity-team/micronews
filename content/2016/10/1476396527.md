Title: Debian will be present at OpenRheinRuhr, Oberhausen, Germany, Nov 5-6 https://lists.debian.org/debian-events-eu/2016/10/msg00004.html
Slug: 1476396527
Date: 2016-10-13 22:08
Author: Ana Guerrero López
Status: published

Debian will be present at OpenRheinRuhr, Oberhausen, Germany, Nov 5-6 [https://lists.debian.org/debian-events-eu/2016/10/msg00004.html](https://lists.debian.org/debian-events-eu/2016/10/msg00004.html)

Title: "softWaves" will be the default theme for Debian 9 https://bits.debian.org/2016/10/softwaves-will-be-the-default-theme-for-debian-9.html
Slug: 1477427742
Date: 2016-10-25 20:35
Author: Laura Arjona Reina
Status: published

"softWaves" will be the default theme for Debian 9 [https://bits.debian.org/2016/10/softwaves-will-be-the-default-theme-for-debian-9.html](https://bits.debian.org/2016/10/softwaves-will-be-the-default-theme-for-debian-9.html)

Title: Our third DebConf16 Gold Sponsor is Collabora
Slug: 1459854040
Date: 2016-04-05 11:00:40
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/vzApFjodSPWxXnodmT7JCA

Our third DebConf16 Gold Sponsor is Collabora. Thanks <a href="https://www.collabora.com/">Collabora</a>! Do you want to <a href="http://debconf16.debconf.org/">sponsor</a> too?

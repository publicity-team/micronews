Title: Bits from Debian: New Debian Developers and Maintainers (January and February 2016) https://bits.debian.org/2016/03/new-developers-2016-02.html
Slug: 1458080738
Date: 2016-03-15 22:25:38
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/OgILcy6URamC0LaMeW10GA

Bits from Debian: New Debian Developers and Maintainers (January and February 2016) [https://bits.debian.org/2016/03/new-developers-2016-02.html](https://bits.debian.org/2016/03/new-developers-2016-02.html)

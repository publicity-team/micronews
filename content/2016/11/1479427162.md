Title: "Google Summer of Code 2016 wrap-up: Debian" By Bruno Magalhães, published in the Google Open Source blog https://opensource.googleblog.com/2016/11/google-summer-of-code-2016-debian.html
Slug: 1479427162
Date: 2016-11-17 23:59
Author: Laura Arjona Reina
Status: published

"Google Summer of Code 2016 wrap-up: Debian" By Bruno Magalhães, published in the Google Open Source blog [https://opensource.googleblog.com/2016/11/google-summer-of-code-2016-debian.html](https://opensource.googleblog.com/2016/11/google-summer-of-code-2016-debian.html)

Title: The transition to OpenSSL 1.1.0 has started in unstable https://lists.debian.org/debian-devel-announce/2016/11/msg00001.html
Slug: 1478095134
Date: 2016-11-02 13:58
Author: Ana Guerrero López
Status: published

The transition to OpenSSL 1.1.0 has started in unstable [https://lists.debian.org/debian-devel-announce/2016/11/msg00001.html](https://lists.debian.org/debian-devel-announce/2016/11/msg00001.html)

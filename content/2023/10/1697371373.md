Title: A Request for Comments [RFC] for the rsyslog.service is open for consideration, we welcome your input and comments. Come and be involved! #debian  https://lists.debian.org/debian-devel/2023/10/msg00055.html
Slug: 1697371373
Date: 2023-10-24 12:02
Author: Donald Norwood
Status: published

A Request for Comments [RFC] for the rsyslog.service is open for consideration, we welcome your input and comments. Come and be involved! #debian  [https://lists.debian.org/debian-devel/2023/10/msg00055.html](https://lists.debian.org/debian-devel/2023/10/msg00055.html)

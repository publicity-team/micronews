Title: Updated Debian 11: 11.8 released https://www.debian.org/News/2023/2023100702
Slug: 1696706514
Date: 2023-10-07 19:21
Author: Jean-Pierre Giraud
Status: published

Updated Debian 11: 11.8 released [https://www.debian.org/News/2023/2023100702](https://www.debian.org/News/2023/2023100702)

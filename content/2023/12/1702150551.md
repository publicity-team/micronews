Title: Due to an issue in ext4 with data corruption in kernel 6.1.64-1, we are pausing the 12.3 image release for today while we attend to fixes. Please do not update any systems at this time, we urge caution for users with UnattendedUpgrades configured. Please see bug# 1057843: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1057843
Slug: 1702150551
Date: 2023-12-09 19:35
Author: Donald Norwood
Status: published

Due to an issue in ext4 with data corruption in kernel 6.1.64-1, we are pausing the 12.3 image release for today while we attend to fixes. Please do not update any systems at this time, we urge caution for users with UnattendedUpgrades configured. Please see bug# 1057843: [https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1057843](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1057843)

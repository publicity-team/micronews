Title: TOR support for Debian bullseye and buster have been marked End-of-Life (EOL). Consider upgrading to bookworm to continue receiving TOR support and updates. https://lists.debian.org/debian-lts/2023/11/msg00019.html
Slug: 1701658911
Date: 2023-12-04 03:01
Author: Jean-Pierre Giraud
Status: published

TOR support for Debian bullseye and buster have been marked End-of-Life (EOL). Consider upgrading to bookworm to continue receiving TOR support and updates. [https://lists.debian.org/debian-lts/2023/11/msg00019.html](https://lists.debian.org/debian-lts/2023/11/msg00019.html)

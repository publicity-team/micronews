Title: Results of the Debian Developer's Survey about Usage of Money in Debian https://lists.debian.org/debian-devel-announce/2023/04/msg00001.html
Slug: 1680692353
Date: 2023-04-05 10:59
Author: Laura Arjona Reina
Status: published

Results of the Debian Developer's Survey about Usage of Money in Debian [https://lists.debian.org/debian-devel-announce/2023/04/msg00001.html](https://lists.debian.org/debian-devel-announce/2023/04/msg00001.html)

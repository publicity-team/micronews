Title: The BoF "Chatting with ftpmasters" intended to smoothen the communication process between ftpmaster and developers will be held in Kuthiran from 16:30 IST to 17:20 IST (11:00 UTC to 11.50 UTC) https://debconf23.debconf.org/talks/31-chatting-with-ftpmasters/ This will also be live streamed. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia
Slug: 1694435878
Date: 2023-09-11 12:37
Author: Anupa Ann Joseph
Status: published

The BoF "Chatting with ftpmasters" intended to smoothen the communication process between ftpmaster and developers will be held in Kuthiran from 16:30 IST to 17:20 IST (11:00 UTC to 11.50 UTC) [https://debconf23.debconf.org/talks/31-chatting-with-ftpmasters/](https://debconf23.debconf.org/talks/31-chatting-with-ftpmasters/) This will also be live streamed. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

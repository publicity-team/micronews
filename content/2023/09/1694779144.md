Title: Begining at 16:30 IST (11:00 UTC), four talks will take place before the afternoon break: "What's new in the Linux kernel (and what's missing in Debian)" in Anamudi, "Uplifting the gross roots using FOSS communities" describes a decade work of an FOSS Community now powering up Debian" in Kuthiran, "A Guided Tour to Debian Installer Development" guide you through the process of how to work on it in Ponmudi, while in Chembra "Past and future changes to the Debian web pages" are evoked https://debconf23.debconf.org/schedule/?block=6 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia
Slug: 1694779144
Date: 2023-09-15 11:59
Author: Jean-Pierre Giraud
Status: published

Begining at 16:30 IST (11:00 UTC), four talks will take place before the afternoon break: "What's new in the Linux kernel (and what's missing in Debian)" in Anamudi, "Uplifting the gross roots using FOSS communities" describes a decade work of an FOSS Community now powering up Debian" in Kuthiran, "A Guided Tour to Debian Installer Development" guide you through the process of how to work on it in Ponmudi, while in Chembra "Past and future changes to the Debian web pages" are evoked [https://debconf23.debconf.org/schedule/?block=6](https://debconf23.debconf.org/schedule/?block=6) #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

Title: The third day of DebConf23 has ended. Tomorrow, Wednesday, September 13, it is Day Trip. Enjoy your Tour! DebConf23 next session starts again Thursday 14, September at 10:30 IST (05:00 UTC). The schedule for the day will be https://debconf23.debconf.org/schedule/?block=5 Thank you to all our contributors, viewers, and to our Debconf Video team! See you tomorrow! #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia
Slug: 1694530852
Date: 2023-09-12 15:00
Author: Jean-Pierre Giraud
Status: published

The third day of DebConf23 has ended. Tomorrow, Wednesday, September 13, it is Day Trip. Enjoy your Tour! DebConf23 next session starts again Thursday 14, September at 10:30 IST (05:00 UTC). The schedule for the day will be [https://debconf23.debconf.org/schedule/?block=5](https://debconf23.debconf.org/schedule/?block=5) Thank you to all our contributors, viewers, and to our Debconf Video team! See you tomorrow! #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

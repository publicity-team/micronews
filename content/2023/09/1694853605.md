Title: The Group photo of #DebConf23 will be taken today after the talks, before the lunch, between 12:30 and 12:45 IST. Please find the instructions on the mail: https://lists.debian.org/debconf-discuss/2023/09/msg00093.html #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia.
Slug: 1694853605
Date: 2023-09-16 08:40
Author: Donald Norwood
Status: published

The Group photo of #DebConf23 will be taken today after the talks, before the lunch, between 12:30 and 12:45 IST. Please find the instructions on the mail: [https://lists.debian.org/debconf-discuss/2023/09/msg00093.html](https://lists.debian.org/debconf-discuss/2023/09/msg00093.html) #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia.

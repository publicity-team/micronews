Title: The second day of DebConf23 has ended, we start again tomorrow, Tuesday 12, September at 10:30 IST (05:00 UTC). The schedule for the day will be [https://debconf23.debconf.org/schedule/?block=3](https://debconf23.debconf.org/schedule/?block=3) Thank you to all our contributors, viewers, and to our Debconf Video team! See you tomorrow! #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia
Slug: 1694447338
Date: 2023-09-11 15:48
Author: Donald Norwood
Status: published

The second day of DebConf23 has ended, we start again tomorrow, Tuesday 12, September at 10:30 IST (05:00 UTC). The schedule for the day will be [https://debconf23.debconf.org/schedule/?block=3](https://debconf23.debconf.org/schedule/?block=3) Thank you to all our contributors, viewers, and to our Debconf Video team! See you tomorrow! #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

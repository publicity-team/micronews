Title: The last set of talks for today at Debconf23: "Empower Your Users to Manage custom Debian Repository" in Kuthiran, and "A beginner's guide to debian packaging" in Ponmudi at 18:00 IST (12:30 UTC)  https://debconf23.debconf.org/schedule/ The talk "DebianArt: Collaborating on Design and Art for Debian Campaigns" in Anamudi has been cancelled and might be rescheduled for later. #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia
Slug: 1694353021
Date: 2023-09-10 13:37
Author: Donald Norwood
Status: published

The last set of talks for today at Debconf23: "Empower Your Users to Manage custom Debian Repository" in Kuthiran, and "A beginner's guide to debian packaging" in Ponmudi at 18:00 IST (12:30 UTC)  [https://debconf23.debconf.org/schedule/](https://debconf23.debconf.org/schedule/) The talk "DebianArt: Collaborating on Design and Art for Debian Campaigns" in Anamudi has been cancelled and might be rescheduled for later. #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

Title: The Debian Developers Conference continues today Monday 11, September, at 10:30 IST (05:00 UTC) - have a look at the schedule for the day https://debconf23.debconf.org/schedule/?block=2 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia
Slug: 1694414318
Date: 2023-09-11 06:38
Author: Anupa Ann Joseph
Status: published

The Debian Developers Conference continues today Monday 11, September, at 10:30 IST (05:00 UTC) - have a look at the schedule for the day [https://debconf23.debconf.org/schedule/?block=2](https://debconf23.debconf.org/schedule/?block=2) #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

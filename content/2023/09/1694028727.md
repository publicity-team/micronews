Title: Siemens: Our Platinum sponsor for DebConf23 Kochi https://bits.debian.org/2023/09/debconf23-siemens-sponsor.html #debian #siemens #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia
Slug: 1694028727
Date: 2023-09-06 19:32
Author: Anupa Ann Joseph
Status: published

Siemens: Our Platinum sponsor for DebConf23 Kochi [https://bits.debian.org/2023/09/debconf23-siemens-sponsor.html](https://bits.debian.org/2023/09/debconf23-siemens-sponsor.html) #debian #siemens #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

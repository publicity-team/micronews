Title: At 15.30 IST (10:00 UTC) the afternoon sessions start with talks in Anamudi "Using FOSS to fight for Digital Freedom", "Teams, newcomers and numbers" in Kuthiran, and "State of Stateless - A Talk about Immutability and Reproducibility in Debian" in Ponmudi. https://debconf23.debconf.org/schedule/ #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia
Slug: 1694344393
Date: 2023-09-10 11:13
Author: Donald Norwood
Status: published

At 15.30 IST (10:00 UTC) the afternoon sessions start with talks in Anamudi "Using FOSS to fight for Digital Freedom", "Teams, newcomers and numbers" in Kuthiran, and "State of Stateless - A Talk about Immutability and Reproducibility in Debian" in Ponmudi. [https://debconf23.debconf.org/schedule/](https://debconf23.debconf.org/schedule/) #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

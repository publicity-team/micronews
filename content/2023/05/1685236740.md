Title: Debian Installer Bookworm RC 4 release https://lists.debian.org/debian-devel-announce/2023/05/msg00003.html
Slug: 1685236740
Date: 2023-05-28 01:19
Author: Jean-Pierre Giraud
Status: published

Debian Installer Bookworm RC 4 release [https://lists.debian.org/debian-devel-announce/2023/05/msg00003.html](https://lists.debian.org/debian-devel-announce/2023/05/msg00003.html)

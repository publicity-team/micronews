Title: Preparations for celebrating the planned release on Sat June 10 of Debian 12 "bookworm" are in full swing. Currently Debian Bookworm Release Parties are planned in 3 continents: Bolivia and Brazil; Belgium, Germany, Netherlands and Portugal; and in Iran.  See https://wiki.debian.org/ReleasePartyBookworm 
Slug: 1685478564
Date: 2023-05-30 20:29
Author: Joost van Baal-Ilić
Status: published

Preparations for celebrating the planned release on Sat June 10 of Debian 12 "bookworm" are in full swing. Currently Debian Bookworm Release Parties are planned in 3 continents: Bolivia and Brazil; Belgium, Germany, Netherlands and Portugal; and in Iran.  See [https://wiki.debian.org/ReleasePartyBookworm](https://wiki.debian.org/ReleasePartyBookworm) 

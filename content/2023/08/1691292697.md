Title: Debian Project Bits - August 5 2023: Debian Day, Events, Reports, Release information, New Members, and Security updates. Issue 1.
Slug: 1691292697
Date: 2023-08-06 03:31
Author: Donald Norwood
Status: published

[Debian Project Bits](https://bits.debian.org/2023/08/debian-project-bits1.html) - August 5 2023: Debian Day, Events, Reports, Release information, New Members, and Security updates. Issue 1. [https://bits.debian.org/2023/08/debian-project-bits1.html](https://bits.debian.org/2023/08/debian-project-bits1.html)

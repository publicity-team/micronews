Title: "Some fun facts about AI and a few thoughts about the software ecosystem trend" by M. Zhou (and info about the Debian Deep Learning team) https://lists.debian.org/debian-project/2023/03/msg00026.html
Slug: 1680092236
Date: 2023-03-29 12:17
Author: Laura Arjona Reina
Status: published

"Some fun facts about AI and a few thoughts about the software ecosystem trend" by M. Zhou (and info about the Debian Deep Learning team) [https://lists.debian.org/debian-project/2023/03/msg00026.html](https://lists.debian.org/debian-project/2023/03/msg00026.html)

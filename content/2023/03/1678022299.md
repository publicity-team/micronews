Title: Debian Project Leader Elections 2023: Call for nominations https://lists.debian.org/debian-devel-announce/2023/03/msg00000.html
Slug: 1678022299
Date: 2023-03-05 13:18
Author: Laura Arjona Reina
Status: published

Debian Project Leader Elections 2023: Call for nominations [https://lists.debian.org/debian-devel-announce/2023/03/msg00000.html](https://lists.debian.org/debian-devel-announce/2023/03/msg00000.html)

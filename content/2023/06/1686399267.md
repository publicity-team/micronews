Title: Don't forget to check your APT source lines carefully for this release. If you refer to the "stable" suite and not the "bullseye" codename, you might get a surprise later today! #ReleasingDebianBookworm
Slug: 1686399267
Date: 2023-06-10 12:14
Author: Jonathan Wiltshire
Status: published

Don't forget to check your APT source lines carefully for this release. If you refer to the "stable" suite and not the "bullseye" codename, you might get a surprise later today! #ReleasingDebianBookworm

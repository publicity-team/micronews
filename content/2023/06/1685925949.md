Title: Bookworm nearly ready for release! Have look at the Bits from the Release Team https://lists.debian.org/debian-devel-announce/2023/06/msg00000.html
Slug: 1685925949
Date: 2023-06-05 00:45
Author: Jean-Pierre Giraud
Status: published

Bookworm nearly ready for release! Have look at the Bits from the Release Team [https://lists.debian.org/debian-devel-announce/2023/06/msg00000.html](https://lists.debian.org/debian-devel-announce/2023/06/msg00000.html)

Title: Updated Debian 10: 10.12 released https://www.debian.org/News/2022/2022032602
Slug: 1648319994
Date: 2022-03-26 18:39
Author: Laura Arjona Reina
Status: published

Updated Debian 10: 10.12 released [https://www.debian.org/News/2022/2022032602](https://www.debian.org/News/2022/2022032602)

Title:  #DebConf22 will be an in-person conference https://debconf22.debconf.org/news/2022-03-09-debconf22-in-person/
Slug: 1646907731
Date: 2022-03-10 10:22
Author: Laura Arjona Reina
Status: published

 #DebConf22 will be an in-person conference [https://debconf22.debconf.org/news/2022-03-09-debconf22-in-person/](https://debconf22.debconf.org/news/2022-03-09-debconf22-in-person/)

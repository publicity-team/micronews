Title: Debian turns 29! #DebianDay #DebianDay2022  https://bits.debian.org/2022/08/debian-turns-29.html
Slug: 1660655002
Date: 2022-08-16 13:03
Author: Laura Arjona Reina
Status: published

Debian turns 29! #DebianDay #DebianDay2022  [https://bits.debian.org/2022/08/debian-turns-29.html](https://bits.debian.org/2022/08/debian-turns-29.html) ![Happy birthday Debian by Juliette Taka](|static|/images/happy_birthday_Debian_29_600x600.png)


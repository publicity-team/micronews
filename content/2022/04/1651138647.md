Title:  #DebConf22 bursary applications and call for papers are closing in less than 72 hours! https://bits.debian.org/2022/04/debconf22-bursaries-and-cfp-last-call.html
Slug: 1651138647
Date: 2022-04-28 09:37
Author: Laura Arjona Reina
Status: published

 #DebConf22 bursary applications and call for papers are closing in less than 72 hours! [https://bits.debian.org/2022/04/debconf22-bursaries-and-cfp-last-call.html](https://bits.debian.org/2022/04/debconf22-bursaries-and-cfp-last-call.html)

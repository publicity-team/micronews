Title: Temporary freezes of udeb-producing packages for Debian installer releases https://lists.debian.org/debian-devel-announce/2022/09/msg00000.html
Slug: 1663633377
Date: 2022-09-20 00:22
Author: Laura Arjona Reina
Status: published

Temporary freezes of udeb-producing packages for Debian installer releases [https://lists.debian.org/debian-devel-announce/2022/09/msg00000.html](https://lists.debian.org/debian-devel-announce/2022/09/msg00000.html)

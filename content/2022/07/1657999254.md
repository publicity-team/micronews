Title: In "How Google got to rolling Linux releases for Desktops", Margarita Manterola and other Debian friends explain the move to a rolling release model based on Debian for the Google internal facing Linux distribution https://cloud.google.com/blog/topics/developers-practitioners/how-google-got-to-rolling-linux-releases-for-desktops
Slug: 1657999254
Date: 2022-07-16 19:20
Author: Laura Arjona Reina
Status: published

In "How Google got to rolling Linux releases for Desktops", Margarita Manterola and other Debian friends explain the move to a rolling release model based on Debian for the Google internal facing Linux distribution [https://cloud.google.com/blog/topics/developers-practitioners/how-google-got-to-rolling-linux-releases-for-desktops](https://cloud.google.com/blog/topics/developers-practitioners/how-google-got-to-rolling-linux-releases-for-desktops)

Title: If you're attending #DebConf22 in person and if you have previous DebConf T-shirts with you, please bring them for the group photo session. See https://wiki.debian.org/DebConf/22/DebconfShirts
Slug: 1658327099
Date: 2022-07-20 14:24
Author: Anupa Ann Joseph
Status: published

If you're attending #DebConf22 in person and if you have previous DebConf T-shirts with you, please bring them for the group photo session. See [https://wiki.debian.org/DebConf/22/DebconfShirts](https://wiki.debian.org/DebConf/22/DebconfShirts)

Title: Want to be hired or hire? A Job Fair is happening today at #DebConf22 https://debconf22.debconf.org/schedule/job-fair/
Slug: 1658056420
Date: 2022-07-17 11:13
Author: Laura Arjona Reina
Status: published

Want to be hired or hire? A Job Fair is happening today at #DebConf22 [https://debconf22.debconf.org/schedule/job-fair/](https://debconf22.debconf.org/schedule/job-fair/)

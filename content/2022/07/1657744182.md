Title: Welcome to the Innovation and Training Park Prizren (ITP) in Kosovo (photo by  Daniel Lenharo) #DebCamp #DebConf22
Slug: 1657744182
Date: 2022-07-13 20:29
Author: Paulo Henrique de Lima Santana (phls)
Status: published

Welcome to the Innovation and Training Park Prizren (ITP) in Kosovo (photo by  Daniel Lenharo) #DebCamp #DebConf22

![ITP building #DebConf22](|static|/images/DebConf22-ITP-entrance.jpg)

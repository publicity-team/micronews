Title: Live at 09:00 UTC in #DebConf22, "pristine-lfs: a robust replacement for pristine-tar using Git LFS" in Drini, "debuginfod.debian.net: current status and next steps" in Lumbardhi and the Local Teams BoF in Ereniku https://debconf22.debconf.org/schedule/?block=7
Slug: 1658572465
Date: 2022-07-23 10:34
Author: Anupa Ann Joseph
Status: published

Live at 09:00 UTC in #DebConf22, "pristine-lfs: a robust replacement for pristine-tar using Git LFS" in Drini, "debuginfod.debian.net: current status and next steps" in Lumbardhi and the Local Teams BoF in Ereniku [https://debconf22.debconf.org/schedule/?block=7](https://debconf22.debconf.org/schedule/?block=7)

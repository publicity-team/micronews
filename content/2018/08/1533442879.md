Title: Last day at #DebConf18. Events start after lunch (see schedule) https://debconf18.debconf.org/schedule/?day=2018-08-05
Slug: 1533442879
Date: 2018-08-05 04:21
Author: Laura Arjona Reina
Status: published

Last day at #DebConf18. Events start after lunch (see schedule) [https://debconf18.debconf.org/schedule/?day=2018-08-05](https://debconf18.debconf.org/schedule/?day=2018-08-05)

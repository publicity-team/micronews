Title:  #DebConf18 is committed to a safe environment for all participants. See our code of conduct: https://debconf.org/codeofconduct.shtml
Slug: 1533298934
Date: 2018-08-03 12:22
Author: Laura Arjona Reina
Status: published

 #DebConf18 is committed to a safe environment for all participants. See our code of conduct: [https://debconf.org/codeofconduct.shtml](https://debconf.org/codeofconduct.shtml)

Title: Room X is hosting the next short talk: "Docker & Go packaging, an overview of the challenges"  https://debconf18.debconf.org/talks/74-docker-go-packaging-an-overview-of-the-challenges/
Slug: 1533273898
Date: 2018-08-03 05:24
Author: Laura Arjona Reina
Status: published

Room X is hosting the next short talk: "Docker & Go packaging, an overview of the challenges"  [https://debconf18.debconf.org/talks/74-docker-go-packaging-an-overview-of-the-challenges/](https://debconf18.debconf.org/talks/74-docker-go-packaging-an-overview-of-the-challenges/)

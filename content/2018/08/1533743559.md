Title: Will you celebrate Debian's 25th birthday on August 16th? Add yourself at https://wiki.debian.org/DebianDay/2018
Slug: 1533743559
Date: 2018-08-08 15:52
Author: Martin Zobel-Helas
Status: published

Will you celebrate Debian's 25th birthday on August 16th? Add yourself at [https://wiki.debian.org/DebianDay/2018](https://wiki.debian.org/DebianDay/2018)

Title: Thanks Aigars Mahinovs, the #DebConf18 group photo is already available https://wiki.debconf.org/wiki/DebConf18/GroupPhoto
Slug: 1533354727
Date: 2018-08-04 03:52
Author: Laura Arjona Reina
Status: published

Thanks Aigars Mahinovs, the #DebConf18 group photo is already available [![DebConf18 group photo](|static|/images/Debconf18_group_photo_small.jpg)](https://wiki.debconf.org/wiki/DebConf18/GroupPhoto)

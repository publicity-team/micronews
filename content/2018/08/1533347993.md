Title: Call for #DebConf20 bids https://lists.debian.org/debconf-announce/2018/08/msg00002.html (some of them will be introduced today during #DebConf18)
Slug: 1533347993
Date: 2018-08-04 01:59
Author: Laura Arjona Reina
Status: published

Call for #DebConf20 bids [https://lists.debian.org/debconf-announce/2018/08/msg00002.html](https://lists.debian.org/debconf-announce/2018/08/msg00002.html) (some of them will be introduced today during #DebConf18)

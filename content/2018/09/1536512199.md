Title: New Debian Developers and Maintainers (July and August 2018) https://bits.debian.org/2018/09/new-developers-2018-08.html
Slug: 1536512199
Date: 2018-09-09 16:56
Author: Laura Arjona Reina
Status: published

New Debian Developers and Maintainers (July and August 2018) [https://bits.debian.org/2018/09/new-developers-2018-08.html](https://bits.debian.org/2018/09/new-developers-2018-08.html)

Title: Bits from the Debian Project Leader (September 2018) -- https://lists.debian.org/debian-devel-announce/2018/09/msg00005.html
Slug: 1538330278
Date: 2018-09-30 17:57
Author: Chris Lamb
Status: published

Bits from the Debian Project Leader (September 2018) -- [https://lists.debian.org/debian-devel-announce/2018/09/msg00005.html](https://lists.debian.org/debian-devel-announce/2018/09/msg00005.html)

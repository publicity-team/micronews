Title: Debian security repositories stay online in Japan despite magnitude 6.7 earthquake -- https://henrich-on-debian.blogspot.com/2018/09/earthquake-struck-hokkaido-and-caused.html
Slug: 1536518145
Date: 2018-09-09 18:35
Author: Chris Lamb
Status: published

Debian security repositories stay online in Japan despite magnitude 6.7 earthquake -- [https://henrich-on-debian.blogspot.com/2018/09/earthquake-struck-hokkaido-and-caused.html](https://henrich-on-debian.blogspot.com/2018/09/earthquake-struck-hokkaido-and-caused.html)

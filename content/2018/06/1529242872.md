Title: Debian Artwork: Call for Proposals for Debian 10 (Buster) https://bits.debian.org/2018/06/buster-artwork-cfp.html
Slug: 1529242872
Date: 2018-06-17 13:41
Author: Laura Arjona Reina
Status: published

Debian Artwork: Call for Proposals for Debian 10 (Buster) [https://bits.debian.org/2018/06/buster-artwork-cfp.html](https://bits.debian.org/2018/06/buster-artwork-cfp.html)

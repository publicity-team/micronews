Title: New Debian Developers and Maintainers (September and October 2018) https://bits.debian.org/2018/11/new-developers-2018-10.html
Slug: 1541713220
Date: 2018-11-08 21:40
Author: Laura Arjona Reina
Status: published

New Debian Developers and Maintainers (September and October 2018) [https://bits.debian.org/2018/11/new-developers-2018-10.html](https://bits.debian.org/2018/11/new-developers-2018-10.html)

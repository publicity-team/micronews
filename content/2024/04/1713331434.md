Title: As Debian Backports do not offer Long Term Support (LTS), buster-backports is soon to be removed from the Debian Archive: https://lists.debian.org/debian-backports-announce/2024/04/msg00000.html #debian
Slug: 1713331434
Date: 2024-04-17 05:23
Author: Donald Norwood
Status: published

As Debian Backports do not offer Long Term Support (LTS), buster-backports is soon to be removed from the Debian Archive: [https://lists.debian.org/debian-backports-announce/2024/04/msg00000.html](https://lists.debian.org/debian-backports-announce/2024/04/msg00000.html) #debian

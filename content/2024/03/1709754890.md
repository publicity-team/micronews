Title: We are not short of MiniDebConfs this year! From March 9th to 10th MiniDebConf Santa Fe will take place in Argentina. For more info, see https://wiki.debian.org/DebianEvents/ar/2024/MiniDebConfSantaFe
Slug: 1709754890
Date: 2024-03-06 19:54
Author: Carlos Henrique Lima Melara
Status: published

We are not short of MiniDebConfs this year! From March 9th to 10th MiniDebConf Santa Fe will take place in Argentina. For more info, see [https://wiki.debian.org/DebianEvents/ar/2024/MiniDebConfSantaFe](https://wiki.debian.org/DebianEvents/ar/2024/MiniDebConfSantaFe)

Title: Mini-DebConf Berlin will take place Tue 14 May until Tue 21 May 2024 at the c-base hackspace in Germany.  Book your trip and lodging now!  RSN a CfP will get published.  Please register at https://wiki.debian.org/DebianEvents/de/2024/MiniDebconfBerlin
Slug: 1711701508
Date: 2024-03-29 08:38
Author: Joost van Baal-Ilić
Status: published

Mini-DebConf Berlin will take place Tue 14 May until Tue 21 May 2024 at the c-base hackspace in Germany.  Book your trip and lodging now!  RSN a CfP will get published.  Please register at [https://wiki.debian.org/DebianEvents/de/2024/MiniDebconfBerlin](https://wiki.debian.org/DebianEvents/de/2024/MiniDebconfBerlin)

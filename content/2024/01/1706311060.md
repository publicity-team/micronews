Title: The DebConf24 Logo Contest is now open for voting! Clean your screens, glasses, squint, or view with your perfect vision these amazing submissions on display for your selection. :)  https://app.formbricks.com/s/clrun50b6cw9xstcjvtull9vm #debian #debconf24 #debconfBusan #korea
Slug: 1706311060
Date: 2024-01-26 23:17
Author: Donald Norwood
Status: published

The DebConf24 Logo Contest is now open for voting! Clean your screens, glasses, squint, or view with your perfect vision these amazing submissions on display for your selection. :)  [https://app.formbricks.com/s/clrun50b6cw9xstcjvtull9vm](https://app.formbricks.com/s/clrun50b6cw9xstcjvtull9vm) #debian #debconf24 #debconfBusan #korea

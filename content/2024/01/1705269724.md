Title: Call for Debian projects and mentors for the Google Summer of Code 2024. https://lists.debian.org/debian-devel-announce/2024/01/msg00001.html
Slug: 1705269724
Date: 2024-01-14 22:02
Author: Donald Norwood
Status: published

Call for Debian projects and mentors for the Google Summer of Code 2024. [https://lists.debian.org/debian-devel-announce/2024/01/msg00001.html](https://lists.debian.org/debian-devel-announce/2024/01/msg00001.html)

Title: MiniDebConf Toulouse 2024 talks start soon. You can find the schedule here https://toulouse2024.mini.debconf.org/schedule/ #debian #debconf #miniDebConfToulouse
Slug: 1731752999
Date: 2024-11-16 10:29
Author: Jean-Pierre Giraud
Status: published

MiniDebConf Toulouse 2024 talks start soon. You can find the schedule here [https://toulouse2024.mini.debconf.org/schedule/](https://toulouse2024.mini.debconf.org/schedule/) #debian #debconf #miniDebConfToulouse

Title: Last slot before the Dinner, at 4:30 PM (KST) (07:30 UTC):  Gnuk 2.2 and Gnuk Next, in Bada, l10n and i18n Bof in Pado and Go team BoF (not streamed) https://debconf24.debconf.org/schedule/?block=9 . #debian #debconf24 #busan #korea #debiankorea
Slug: 1722244592
Date: 2024-07-29 09:16
Author: Jean-Pierre Giraud
Status: published

Last slot before the Dinner, at 4:30 PM (KST) (07:30 UTC):  Gnuk 2.2 and Gnuk Next, in Bada, l10n and i18n Bof in Pado and Go team BoF (not streamed) [https://debconf24.debconf.org/schedule/?block=9](https://debconf24.debconf.org/schedule/?block=9) . #debian #debconf24 #busan #korea #debiankorea

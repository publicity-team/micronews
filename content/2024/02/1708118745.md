Title: [SUA 252-1] Updated nVidia driver packages. nVidia kernel modules via DKMS could not be resolved in time for the release of Debian 12.5, several packages have been updated to correct this issue. Additional information and update instructions: https://lists.debian.org/debian-stable-announce/2024/02/msg00002.html
Slug: 1708118745
Date: 2024-02-16 21:25
Author: Donald Norwood
Status: published

[SUA 252-1] Updated nVidia driver packages. nVidia kernel modules via DKMS could not be resolved in time for the release of Debian 12.5, several packages have been updated to correct this issue. Additional information and update instructions: [https://lists.debian.org/debian-stable-announce/2024/02/msg00002.html](https://lists.debian.org/debian-stable-announce/2024/02/msg00002.html)

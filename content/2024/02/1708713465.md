Title: SCaLE, the largest community-run open-source and free software conference in North America, will take place for the 21st time on March 14-17, 2024 in Pasadena, CA, USA.  Dima Kogan and other Debian enthusiasts will be representing Debian with a booth again.  Use the "DEB50" promo code to get a discount.  Come by and say hi!  https://www.socallinuxexpo.org/scale/21x/exhibitors?page=1
Slug: 1708713465
Date: 2024-02-23 18:37
Author: Joost van Baal-Ilić
Status: published

SCaLE, the largest community-run open-source and free software conference in North America, will take place for the 21st time on March 14-17, 2024 in Pasadena, CA, USA.  Dima Kogan and other Debian enthusiasts will be representing Debian with a booth again.  Use
the "DEB50" promo code to get a discount.  Come by and say hi!  [https://www.socallinuxexpo.org/scale/21x/exhibitors?page=1](https://www.socallinuxexpo.org/scale/21x/exhibitors?page=1)

Title: Today Saturday 02, August, is the last day of DebConf24, starting at 10:00 AM KST (01:00 UTC)! Have a look at the schedule for the day https://debconf24.debconf.org/schedule/?block=14 . We remind you that you can watch most of the talks streamed online by clicking on Venue names in the schedule.
Slug: 1722647682
Date: 2024-08-03 01:14
Author: Jean-Pierre Giraud
Status: published

Today Saturday 03, August, is the last day of DebConf24, starting at 10:00 AM KST (01:00 UTC)! Have a look at the schedule for the day [https://debconf24.debconf.org/schedule/?block=14](https://debconf24.debconf.org/schedule/?block=14) . We remind you that you can watch most of the talks streamed online by clicking on Venue names in the schedule.

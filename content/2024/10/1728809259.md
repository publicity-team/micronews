Title: Video's from the first half of MiniDebConf Cambridge are now available from https://meetings-archive.debian.net/pub/debian-meetings/2024/MiniDebConf-Cambridge/ . #debian #debconf #miniDebConfCambridge
Slug: 1728809259
Date: 2024-10-13 08:47
Author: Joost van Baal-Ilić
Status: published

Videos from the first half of MiniDebConf Cambridge are now available from [https://meetings-archive.debian.net/pub/debian-meetings/2024/MiniDebConf-Cambridge/](https://meetings-archive.debian.net/pub/debian-meetings/2024/MiniDebConf-Cambridge/) . #debian #debconf #miniDebConfCambridge

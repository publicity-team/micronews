Title: MiniDebConf in Aberdeen (Scotland), 24-25th April 2020 https://wiki.debian.org/DebianEvents/gb/2020/MiniDebConfAberdeen
Slug: 1578578305
Date: 2020-01-09 13:58
Author: Laura Arjona Reina
Status: published

MiniDebConf in Aberdeen (Scotland), 24-25th April 2020 [https://wiki.debian.org/DebianEvents/gb/2020/MiniDebConfAberdeen](https://wiki.debian.org/DebianEvents/gb/2020/MiniDebConfAberdeen)

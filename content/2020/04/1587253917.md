Title: Recommendations around Git Packaging in Debian https://lists.debian.org/debian-devel-announce/2020/04/msg00009.html
Slug: 1587253917
Date: 2020-04-18 23:51
Author: Laura Arjona Reina
Status: published

Recommendations around Git Packaging in Debian [https://lists.debian.org/debian-devel-announce/2020/04/msg00009.html](https://lists.debian.org/debian-devel-announce/2020/04/msg00009.html)

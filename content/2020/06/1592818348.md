Title: Language-specific tracks at #DebConf20 online https://lists.debian.org/debian-devel-announce/2020/06/msg00005.html
Slug: 1592818348
Date: 2020-06-22 09:32
Author: Laura Arjona Reina
Status: published

Language-specific tracks at #DebConf20 online [https://lists.debian.org/debian-devel-announce/2020/06/msg00005.html](https://lists.debian.org/debian-devel-announce/2020/06/msg00005.html)

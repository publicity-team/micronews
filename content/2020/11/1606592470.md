Title: A próxima atividade da MiniDebConf Online Brasil será Cuidados para o uso de SSD no Linux, com Eriberto Mota. Assista online a partir das 16h20 no site do evento [https://mdcobr2020.debian.net/schedule/venue/1/](https://mdcobr2020.debian.net/schedule/venue/1/) #Debian #MDCObr2020 #MDCOBR #MiniDebConf
Slug: 1606592470
Date: 2020-11-28 19:42
Author: Paulo Henrique de Lima Santana (phls)
Status: published

A próxima atividade da MiniDebConf Online Brasil será Cuidados para o uso de SSD no Linux, com Eriberto Mota. Assista online a partir das 16h20 no site do evento [https://mdcobr2020.debian.net/schedule/venue/1/](https://mdcobr2020.debian.net/schedule/venue/1/) #Debian #MDCObr2020 #MDCOBR #MiniDebConf

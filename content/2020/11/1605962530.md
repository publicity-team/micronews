Title: First block of short talks at #MDCO2Gaming :  "./play.it, a packages generator for DRM-free games" (12:00 UTC), "YIRL engine presentation" (12:30 UTC) and "NetHack" (13:00 UTC). Follow the live streaming at https://mdco2.mini.debconf.org/schedule/venue/3/
Slug: 1605962530
Date: 2020-11-21 12:42
Author: Anupa Ann Joseph
Status: published

First block of short talks at #MDCO2Gaming :  "./play.it, a packages generator for DRM-free games" (12:00 UTC), "YIRL engine presentation" (12:30 UTC) and "NetHack" (13:00 UTC). Follow the live streaming at [https://mdco2.mini.debconf.org/schedule/venue/3/](https://mdco2.mini.debconf.org/schedule/venue/3/)

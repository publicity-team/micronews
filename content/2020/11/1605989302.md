Title: Last talks for today at MiniDebConf Online #2 Gaming Edition: "FOSS Virtual & Augmented Reality" at 19:30 UTC and "Open Source Game Achievements" at 20:30 UTC  https://mdco2.mini.debconf.org/schedule/venue/3/ #debiangaming #MDCO2Gaming
Slug: 1605989302
Date: 2020-11-21 20:08
Author: Anupa Ann Joseph
Status: published

Last talks for today at MiniDebConf Online #2 Gaming Edition: "FOSS Virtual & Augmented Reality" at 19:30 UTC and "Open Source Game Achievements" at 20:30 UTC  [https://mdco2.mini.debconf.org/schedule/venue/3/](https://mdco2.mini.debconf.org/schedule/venue/3/) #debiangaming #MDCO2Gaming

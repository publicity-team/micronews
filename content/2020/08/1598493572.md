Title: Day 4 of #DebConf2020 has ended. Tune in again tomorrow Thursday, August 27 at 10:00 UTC. The schedule for the day is https://debconf20.debconf.org/schedule/?block=5 Thank you to all of our contributors, viewers, and to our Debconf Video team! See you tomorrow!
Slug: 1598493572
Date: 2020-08-27 02:00
Author: Donald Norwood
Status: published

Day 4 of #DebConf2020 has ended. Tune in again tomorrow Thursday, August 27 at 10:00 UTC. The schedule for the day is [https://debconf20.debconf.org/schedule/?block=5](https://debconf20.debconf.org/schedule/?block=5) Thank you to all of our contributors, viewers, and to our Debconf Video team! See you tomorrow!

Title: Many of the streamed sessions from #DebConf20 are already uploaded in https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/ and the remaining ones will appear in the following days. Thank you again from all of us at Debian!
Slug: 1598754688
Date: 2020-08-30 02:31
Author: Donald Norwood
Status: published

Many of the streamed sessions from #DebConf20 are already uploaded in [https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/) and the remaining ones will appear in the following days. Thank you again from all of us at Debian!

Title: Debian 10.5 CD and other Images available for download. This 10.5 point release also addresses the GRUB2 UEFI SecureBoot 'BootHole' vulnerability. https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot/
Slug: 1596356201
Date: 2020-08-02 08:16
Author: Donald Norwood
Status: published

Debian 10.5 CD and other Images available for download. This 10.5 point release also addresses the GRUB2 UEFI SecureBoot 'BootHole' vulnerability. [https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot/](https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot/)

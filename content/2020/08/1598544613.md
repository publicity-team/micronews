Title: After the break #DebConf20 continues at 14:00 UTC with the "Android Tools BoF "  followed by the ad hoc talk  " Designing trends for cloud-native apps with MySQL " at 15:00 UTC  https://debconf20.debconf.org/
Slug: 1598544613
Date: 2020-08-27 16:10
Author: Laura Arjona Reina
Status: published

After the break #DebConf20 continues at 14:00 UTC with the "Android Tools BoF "  followed by the ad hoc talk  " Designing trends for cloud-native apps with MySQL " at 15:00 UTC  [https://debconf20.debconf.org/](https://debconf20.debconf.org/)

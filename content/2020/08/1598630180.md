Title: At 14:00 UTC there will be an ad-hoc session about "Linux at the Munich city administration" #LiMux #DebConf20 https://debconf20.debconf.org/talks/108-ad-hoc-linux-at-the-munich-city-administration/
Slug: 1598630180
Date: 2020-08-28 15:56
Author: Laura Arjona Reina
Status: published

At 14:00 UTC there will be an ad-hoc session about "Linux at the Munich city administration" #LiMux #DebConf20 [https://debconf20.debconf.org/talks/108-ad-hoc-linux-at-the-munich-city-administration/](https://debconf20.debconf.org/talks/108-ad-hoc-linux-at-the-munich-city-administration/)

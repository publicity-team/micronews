Title: The "Debian Security Essentials" talk at 14:00 UTC has been cancelled. Please stay tuned for our next talk "What comes after Open Source?" at 15:00 UTC https://debconf20.debconf.org/talks/10-what-comes-after-open-source/
Slug: 1598285757
Date: 2020-08-24 16:15
Author: urbec
Status: published

The #DebConf20 "Debian Security Essentials" talk at 14:00 UTC has been cancelled. Please stay tuned for our next talk "What comes after Open Source?" at 15:00 UTC [https://debconf20.debconf.org/talks/10-what-comes-after-open-source/](https://debconf20.debconf.org/talks/10-what-comes-after-open-source/)

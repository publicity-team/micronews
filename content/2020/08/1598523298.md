Title: The first #DebConf20 videos are initially being published at the Debian video archive. As new videos become ready, they will be uploaded to the following location: https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/
Slug: 1598523298
Date: 2020-08-27 10:14
Author: Laura Arjona Reina
Status: published

The first #DebConf20 videos are initially being published at the Debian video archive. As new videos become ready, they will be uploaded to the following location: [https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/)

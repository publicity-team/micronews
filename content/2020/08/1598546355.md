Title: Today, from 15:00 to 17:00 UTC #DebConf20 online speaks Malayalam! Talks are: State of writing Malayalam on GNU/Linux : ഗ്നു/ലിനക്സിലെ മലയാളമെഴുത്തു്‌ (15:00 UTC), എന്താണ് ഡെബിയൻ? (15:30 UTC) and എനിയ്ക്കും ഡെബിയനില്‍ വരണം, ഞാന്‍ എന്തു് ചെയ്യണം? (I want to join Debian, what should I do?) (16:00 UTC) https://debconf20.debconf.org/schedule/?block=5
Slug: 1598546355
Date: 2020-08-27 16:39
Author: Laura Arjona Reina
Status: published

Today, from 15:00 to 17:00 UTC #DebConf20 online speaks Malayalam! Talks are: State of writing Malayalam on GNU/Linux : ഗ്നു/ലിനക്സിലെ മലയാളമെഴുത്തു്‌ (15:00 UTC), എന്താണ് ഡെബിയൻ? (15:30 UTC) and എനിയ്ക്കും ഡെബിയനില്‍ വരണം, ഞാന്‍ എന്തു് ചെയ്യണം? (I want to join Debian, what should I do?) (16:00 UTC) [https://debconf20.debconf.org/schedule/?block=5](https://debconf20.debconf.org/schedule/?block=5)

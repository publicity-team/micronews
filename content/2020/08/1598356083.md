Title: The first talk at #DebConf20 starts at 10:00 UTC: "Experiment about Debian's bug tracking front-end" and it will be followeb by "Deep Learning & Accelerated Calculation with Debian" at 10:30 UTC. Follow the live streaming at https://debconf20.debconf.org/schedule/venue/1/
Slug: 1598356083
Date: 2020-08-25 11:48
Author: Laura Arjona Reina
Status: published

The first talk at #DebConf20 starts at 10:00 UTC: "Experiment about Debian's bug tracking front-end" and it will be followeb by "Deep Learning & Accelerated Calculation with Debian" at 10:30 UTC. Follow the live streaming at [https://debconf20.debconf.org/schedule/venue/1/](https://debconf20.debconf.org/schedule/venue/1/)

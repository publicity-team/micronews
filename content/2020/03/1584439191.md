Title: Bits: new policy on out-of-sync and Release Team composition https://lists.debian.org/debian-devel-announce/2020/02/msg00005.html
Slug: 1584439191
Date: 2020-03-17 09:59
Author: Laura Arjona Reina
Status: published

Bits: new policy on out-of-sync and Release Team composition [https://lists.debian.org/debian-devel-announce/2020/02/msg00005.html](https://lists.debian.org/debian-devel-announce/2020/02/msg00005.html)

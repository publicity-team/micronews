Title: Salsa CI now includes i386 build support https://bits.debian.org/2020/10/salsa-ci-i386-build.html
Slug: 1602274944
Date: 2020-10-09 20:22
Author: Laura Arjona Reina
Status: published

Salsa CI now includes i386 build support [https://bits.debian.org/2020/10/salsa-ci-i386-build.html](https://bits.debian.org/2020/10/salsa-ci-i386-build.html)

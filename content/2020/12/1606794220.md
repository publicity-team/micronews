Title: The MiniDebConf Brasil Online 2020 talks are available! https://lists.debian.org/debian-project/2020/11/msg00019.html #Debian #MDCObr2020 #MDCOBR #MiniDebConf
Slug: 1606794220
Date: 2020-12-01 03:43
Author: Donald Norwood
Status: published

The MiniDebConf Brasil Online 2020 talks are available! [https://lists.debian.org/debian-project/2020/11/msg00019.html](https://lists.debian.org/debian-project/2020/11/msg00019.html) #Debian #MDCObr2020 #MDCOBR #MiniDebConf

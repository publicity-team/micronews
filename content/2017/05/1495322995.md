Title: "Debian stretch: changes in util-linux #newinstretch" by Michael Prokop https://michael-prokop.at/blog/2017/05/19/debian-stretch-changes-in-util-linux-newinstretch/
Slug: 1495322995
Date: 2017-05-20 23:29
Author: Laura Arjona Reina
Status: published

"Debian stretch: changes in util-linux #newinstretch" by Michael Prokop [https://michael-prokop.at/blog/2017/05/19/debian-stretch-changes-in-util-linux-newinstretch/](https://michael-prokop.at/blog/2017/05/19/debian-stretch-changes-in-util-linux-newinstretch/)

Title: Debian update 8.8 point release https://www.debian.org/News/2017/20170506
Slug: 1494096021
Date: 2017-05-06 18:40
Author: Donald Norwood
Status: published

Debian update 8.8 point release [https://www.debian.org/News/2017/20170506](https://www.debian.org/News/2017/20170506)

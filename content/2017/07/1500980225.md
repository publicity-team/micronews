Title: DebConf17 schedule published! https://bits.debian.org/2017/07/dc17-schedule.html
Slug: 1500980225
Date: 2017-07-25 10:57
Author: Laura Arjona Reina
Status: published

DebConf17 schedule published! [https://bits.debian.org/2017/07/dc17-schedule.html](https://bits.debian.org/2017/07/dc17-schedule.html)

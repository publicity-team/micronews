Title: And more BoFs at DebConf17: Debian Electronics and Debian Diversity Group BoFs just finished, SPI BOF will start https://debconf17.debconf.org/schedule/?day=2017-08-10
Slug: 1502409615
Date: 2017-08-11 00:00
Author: Laura Arjona Reina
Status: published

And more BoFs at DebConf17: Debian Electronics and Debian Diversity Group BoFs just finished, SPI BOF will start [https://debconf17.debconf.org/schedule/?day=2017-08-10](https://debconf17.debconf.org/schedule/?day=2017-08-10)

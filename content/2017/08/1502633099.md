Title: You missed a talk given at DebConf17? No problem, most of them were recorded and are available at http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17
Slug: 1502633099
Date: 2017-08-13 14:04
Author: Martin Zobel-Helas
Status: published

You missed a talk given at DebConf17? No problem, most of them were recorded and are available at [http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17](http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17)

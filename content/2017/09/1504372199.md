Title: DebConf17 videos, now with RSS file (so your podcast player will be able to play it). Thanks Debian Video Team! https://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/
Slug: 1504372199
Date: 2017-09-02 17:09
Author: Laura Arjona Reina
Status: published

DebConf17 videos, now with RSS file (so your podcast player will be able to play it). Thanks Debian Video Team! [https://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/](https://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/)

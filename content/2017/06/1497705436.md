Title: Debian Stretch will ship with the first release of the Debian Astro Pure Blend https://blends.debian.org/astro #releasingstretch
Slug: 1497705436
Date: 2017-06-17 13:17
Author: Laura Arjona Reina
Status: published

Debian Stretch will ship with the first release of the Debian Astro Pure Blend [https://blends.debian.org/astro](https://blends.debian.org/astro) #releasingstretch

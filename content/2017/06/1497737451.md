Title: There are 417 Debian consultants in 55 countries worldwide. Consult on Debian today! https://www.debian.org/consultants/
Slug: 1497737451
Date: 2017-06-17 22:10
Author: Cédric Boutillier
Status: published

There are 417 Debian consultants in 55 countries worldwide. Consult on Debian today! [https://www.debian.org/consultants/](https://www.debian.org/consultants/)

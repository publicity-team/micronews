Title: There have been about 30 different official and unofficial ports to various kernels and hardware types https://www.debian.org/ports/ #releasingstretch
Slug: 1497767270
Date: 2017-06-18 06:27
Author: Paul Wise
Status: published

There have been about 30 different official and unofficial ports to various kernels and hardware types [https://www.debian.org/ports/](https://www.debian.org/ports/) #releasingstretch

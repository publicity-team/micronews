Title: Many popular MELPA packages (Emacs Lisp) now installable with apt, such as Flycheck, Projectile, Evil and Helm #releasingstretch
Slug: 1497717073
Date: 2017-06-17 16:31
Author: Paul Wise
Status: published

Many popular MELPA packages (Emacs Lisp) now installable with apt, such as Flycheck, Projectile, Evil and Helm #releasingstretch

Title: The first stretch point release will be in about 1 month. Unstable is open and testing migration opens soonish https://lists.debian.org/msgid-search/20170618064919.zthnb6wxbfo4aada@powdarrmonkey.net #releasingstretch
Slug: 1497776726
Date: 2017-06-18 09:05
Author: Paul Wise
Status: published

The first stretch point release will be in about 1 month. Unstable is open and testing migration opens soonish [https://lists.debian.org/msgid-search/20170618064919.zthnb6wxbfo4aada@powdarrmonkey.net](https://lists.debian.org/msgid-search/20170618064919.zthnb6wxbfo4aada@powdarrmonkey.net) #releasingstretch

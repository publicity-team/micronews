Title: When upgrading from Jessie to Stretch, it can be a good idea to purge old packages before the first reboot. #releasingstretch
Slug: 1497790329
Date: 2017-06-18 12:52
Author: Paul Wise
Status: published

When upgrading from Jessie to Stretch, it can be a good idea to purge old packages before the first reboot. #releasingstretch

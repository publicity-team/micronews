Title: Contribute *your* skills to Debian in Paris, May 13-14 2017 https://wiki.debian.org/BSP/2017/05/fr/Paris/Announcement
Slug: 1488822806
Date: 2017-03-06 17:53
Author: spriver
Status: published

Contribute *your* skills to Debian in Paris, May 13-14 2017 [https://wiki.debian.org/BSP/2017/05/fr/Paris/Announcement](https://wiki.debian.org/BSP/2017/05/fr/Paris/Announcement)

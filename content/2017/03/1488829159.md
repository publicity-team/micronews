Title: No Google Summer of Code for Debian in 2017, but we continue participating in Outreachy https://wiki.debian.org/Outreachy/Round14
Slug: 1488829159
Date: 2017-03-06 19:39
Author: Laura Arjona Reina
Status: published

No Google Summer of Code for Debian in 2017, but we continue participating in Outreachy [https://wiki.debian.org/Outreachy/Round14](https://wiki.debian.org/Outreachy/Round14)

Title: Debian is shutting down its FTP public services https://www.debian.org/News/2017/20170425
Slug: 1493135528
Date: 2017-04-25 15:52
Author: Cédric Boutillier
Status: published

Debian is shutting down soon its public FTP services [https://www.debian.org/News/2017/20170425](https://www.debian.org/News/2017/20170425)

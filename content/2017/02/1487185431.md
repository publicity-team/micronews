Title: De-Branding of Icedove, reintroducing Thunderbird packages into Debian https://lists.debian.org/debian-devel-announce/2017/02/msg00004.html
Slug: 1487185431
Date: 2017-02-15 19:03
Author: Laura Arjona Reina
Status: published

De-Branding of Icedove, reintroducing Thunderbird packages into Debian [https://lists.debian.org/debian-devel-announce/2017/02/msg00004.html](https://lists.debian.org/debian-devel-announce/2017/02/msg00004.html)

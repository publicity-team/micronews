This repository contains all the sources needed to maintain the Debian
micronews service https://micronews.debian.org and instructions to build
the site.

## Contents

		Makefile
		pelicanconf.py  configuration file for pelican
		publishconf.py  publishing configuration file for pelican
		add.py          script used to add micronews easily
		theme-micro/    contains the blog's theme and the templates used.
		content/        contains the source for the contents
		content/pages/  contains the static pages
		content/2013/   contains the blog posts for 2013
		output/         should be always empty, it is for generate the blog locally
## Add micronews

Use the add.py script in order to create a new micronews post. For example:

		$ ./add.py -a "Ana Guerrero López" -t "Debian and Tor Services available as Onion Services https://bits.debian.org/2016/08/debian-and-tor-services-available-as-onion-services.html"

See https://micronews.debian.org/pages/contribute.html for further information
about contributing to micronews.

## Testing locally

* Install pelican

* Clone this repository

		$ git clone https://salsa.debian.org/publicity-team/micronews.git && cd micronews

* Build the site:

		$ make html

* Serve it locally to see if it's rendered properly:

		$ make serve

* Open <http://localhost:8000> in your browser.


## Updating the blog in dillon.d.o

This is only valid for people in the publicity group (the group of Linux
users in Debian machines).

* Log in dillon.debian.org and get a copy of the micronews repository
or update your current copy:

		$ git clone https://salsa.debian.org/publicity-team/micronews.git

* Execute:

		$ cd micronews
		$ make mpublish

This will remove all the files at `/srv/micronews.debian.org/htdocs`
and will regenerate a new copy of the website and propagate update
to all the static mirrors.
